import java.util.Scanner;

/**
* @author Mehmet ACIKGOZ
*/

public class Project1 {

	/*
	 * CS205 - Project 1 
	 * 
	 * This program calculates the number of miles per gallon and the number of kilometers per liter when 
	 *  given gallons used and starting and ending odometer readings. 
	 *  The results are displayed to the user.
	 *  
	 */
	public static void main(String [] args) {
		
		Scanner input = new Scanner(System.in);
		
		final double KM_PER_MILE = 1.60934;
		final double LITER_PER_GALLON = 3.78541;
		
		System.out.print("Please enter number of gallons used: ");
		double gallons_used = input.nextDouble();
		
		if (gallons_used > 0) {
			System.out.print("Please enter the beginning odometer reading: ");
			double starting_odometer = input.nextDouble();
			
			System.out.print("Please enter the ending odometer reading: ");
			double ending_odometer = input.nextDouble();
			
			if (ending_odometer > starting_odometer) {
				//System.out.println("");
				double miles_per_gallons = (ending_odometer - starting_odometer) / gallons_used;
				double km_per_liters = miles_per_gallons * KM_PER_MILE / LITER_PER_GALLON;
				System.out.printf("\nThe number of miles per gallons is: %.4f",  miles_per_gallons);
				System.out.printf("\nThe number of kilometers per liter is: %.4f", km_per_liters );				
			}
			else {
				System.out.print("I'm sorry, the ending odometer reading must be greater than the starting odometer reading.");				
			}
			
		}
		else {
			System.out.print("I'm sorry, gallons used must be greater than 0.");
		}	

	}
}
